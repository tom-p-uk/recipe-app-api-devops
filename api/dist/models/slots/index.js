"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const dbClient = require('../../store/db');
const getSlots = (offset = 0, limit = 3) => {
    const query = `
        SELECT *, count(*) OVER() AS full_count
        FROM slots
        WHERE EXTRACT(dow FROM date) NOT IN (0)
        ORDER BY date ASC
        OFFSET $1
        LIMIT $2
    `;
    return dbClient.query(query, [offset * limit, limit]);
};
const getFinalSlots = (limit = 3) => {
    const query = `
        SELECT *, count(*) OVER() AS full_count
        FROM slots
        WHERE EXTRACT(dow FROM date) NOT IN (0)
        ORDER BY date DESC
        LIMIT $1
    `;
    return dbClient.query(query, [limit]);
};
const selectSlot = (id, time) => {
    const query = `
        UPDATE slots
        SET ${time} = NOT ${time}
        WHERE slots.id = $1
        RETURNING *;
    `;
    return dbClient.query(query, [id]);
};
const selectSpecialDelivery = () => {
    const query = `
        UPDATE slots
        SET special_delivery = NOT special_delivery
        WHERE EXTRACT(dow FROM date) IN (3)
        RETURNING *;
    `;
    return dbClient.query(query);
};
exports.default = {
    getSlots,
    getFinalSlots,
    selectSlot,
    selectSpecialDelivery
};
//# sourceMappingURL=index.js.map