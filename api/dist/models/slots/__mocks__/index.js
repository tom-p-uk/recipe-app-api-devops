"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const generateMockData = require('../../../util/generateMockData');
const getSlots = jest.fn(() => {
    return new Promise((resolve) => {
        const rows = generateMockData(3, true);
        return resolve({ rows });
    });
});
const getFinalSlots = jest.fn(() => {
    return new Promise((resolve) => {
        const rows = generateMockData(3, true);
        return resolve({ rows });
    });
});
exports.default = {
    getSlots,
    getFinalSlots
};
//# sourceMappingURL=index.js.map