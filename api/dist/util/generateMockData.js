"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const moment_1 = __importDefault(require("moment"));
const generateMockData = (numToGenerate, makeSlotsAvailable) => {
    const rows = [];
    for (let i = 1; i <= numToGenerate; i++) {
        const row = {
            id: i,
            date: moment_1.default().add(i, 'days').format('YYYY-MM-DD'),
            am: makeSlotsAvailable,
            pm: makeSlotsAvailable,
            eve: makeSlotsAvailable,
            special_delivery: false,
            is_second_friday: false,
        };
        rows.push(row);
    }
    ;
    return rows;
};
exports.default = generateMockData;
//# sourceMappingURL=generateMockData.js.map