"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const slots_1 = __importDefault(require("./slots"));
const router = express_1.Router();
router.get('/slots', slots_1.default.get);
router.put('/slots', slots_1.default.put);
router.put('/slots/:id', slots_1.default.putId);
exports.default = router;
//# sourceMappingURL=index.js.map