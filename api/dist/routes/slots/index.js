"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const slots_1 = __importDefault(require("../../services/slots"));
const get = (req, res) => {
    const { offset, limit } = req.query;
    return slots_1.default.getSlots(parseInt(offset), parseInt(limit))
        .then((result) => res.status(200).send(result))
        .catch((err) => res.status(500).send({ error: err.message }));
};
const putId = (req, res) => {
    const { id } = req.params;
    const { time } = req.body;
    return slots_1.default.selectSlot(id, time)
        .then((result) => res.status(200).send(result))
        .catch((err) => res.status(500).send({ error: err.message }));
};
const put = (req, res) => {
    return slots_1.default.selectSpecialDelivery()
        .then((result) => res.status(200).send(result))
        .catch((err) => res.status(500).send({ error: err.message }));
};
exports.default = {
    get,
    put,
    putId
};
//# sourceMappingURL=index.js.map