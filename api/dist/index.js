"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const body_parser_1 = __importDefault(require("body-parser"));
const cors_1 = __importDefault(require("cors"));
const morgan_1 = __importDefault(require("morgan"));
const routes_1 = __importDefault(require("./routes"));
const db_1 = __importDefault(require("./store/db"));
const seed_1 = __importDefault(require("./db/seed"));
const PORT = process.env.APP_PORT || 8000;
const app = express_1.default();
db_1.default.connect()
    .then(() => seed_1.default())
    .then((seedSuccessMsg) => console.log(`Connected to DB and ${seedSuccessMsg}`))
    .catch((err) => console.log(err));
app.use(morgan_1.default('combined'));
app.use(cors_1.default());
app.use(body_parser_1.default.json());
app.use(routes_1.default);
app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
//# sourceMappingURL=index.js.map