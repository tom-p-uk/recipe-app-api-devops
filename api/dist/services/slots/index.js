"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const slots_1 = __importDefault(require("../../models/slots"));
const errors_1 = require("../../errors");
const getSlots = (offset, limit) => {
    return slots_1.default.getSlots(offset, limit)
        .then((result) => {
        if (result.rows.length === limit) {
            return removeFirstDayIfNoSlots(result.rows);
        }
        return slots_1.default.getFinalSlots(limit)
            .then((result) => removeFirstDayIfNoSlots(result.rows.reverse()));
    })
        .catch((err) => {
        console.log('ERROR:', err);
        throw errors_1.DBReadException;
    });
};
const removeFirstDayIfNoSlots = (data) => {
    const first = data[0];
    if (!first.am && !first.pm && !first.eve) {
        return data.slice(1);
    }
    return data;
};
const selectSlot = (id, time) => {
    return slots_1.default.selectSlot(id, time)
        .then((result) => result.rows)
        .catch((err) => {
        console.log('ERROR:', err);
        throw errors_1.DBUpdateException;
    });
};
const selectSpecialDelivery = () => {
    return slots_1.default.selectSpecialDelivery()
        .then((result) => result.rows)
        .catch((err) => {
        console.log('ERROR:', err);
        throw errors_1.DBUpdateException;
    });
};
exports.default = {
    getSlots,
    removeFirstDayIfNoSlots,
    selectSlot,
    selectSpecialDelivery
};
//# sourceMappingURL=index.js.map