"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DBWriteException = exports.DBUpdateException = exports.DBReadException = exports.DBSeedException = void 0;
exports.DBSeedException = new Error('DB could not be seeded at this time.');
exports.DBReadException = new Error('There was a problem retrieving results from the DB.');
exports.DBUpdateException = new Error('There was a problem updating the DB.');
exports.DBWriteException = new Error('There was a problem writing to the DB.');
//# sourceMappingURL=index.js.map