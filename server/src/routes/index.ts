import {Router} from 'express';
import dbClient from '../store/dbClient';

const router = Router();

router.get('/todos', async (req, res) => {
    const query = 'SELECT * from todos;';

    try {
        const {rows} = await dbClient.query(query);
        res.status(200).json(rows);
    } catch (err) {
        console.error('ERROR:', err);
        res.status(500).json({error: 'Unable to retrieve items'});
    }
});

router.post('/todos', (req, res) => {
    console.log('\n\n', req, '\n\n')
    const {title} = req.body;
    const query = `
        INSERT INTO todos (title)
        VALUES ($1)
        RETURNING *;
    `;

    dbClient.query(query, [title])
        .then((result) => {
            res.status(201).json(result.rows);
        })
        .catch((err) => {
            console.error('ERROR:', err);
            res.status(500).json({error: 'Unable to create new item'});
        });
});

router.patch('/todos/:id', (req, res) => {
    const {id} = req.params;

    const query = `
        UPDATE todos
        SET completed = != completed
        WHERE id = $1
        RETURNING *;
    `;

    dbClient.query(query, [id])
        .then((result) => {
            res.status(204).json(result.rows);
        })
        .catch((err) => {
            console.error('ERROR:', err);
            res.status(500).json({error: 'Unable to delete item'});
        });
});

router.delete('/todos/:id', (req, res) => {
    const {id} = req.params;

    const query = `
        DELETE * from todos WHERE id = $1
        RETURNING *;
    `;

    dbClient.query(query, [id])
        .then((result) => {
            res.status(204).json(result.rows);
        })
        .catch((err) => {
            console.error('ERROR:', err);
            res.status(500).json({error: 'Unable to delete item'});
        });
});

export default router;
