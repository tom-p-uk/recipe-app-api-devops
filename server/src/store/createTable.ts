import dbClient from './dbClient';

const createTableQuery = `
    CREATE TABLE IF NOT EXISTS todos (
        id SERIAL,
        title TEXT
    );
`;

const createTable = () => dbClient.query(createTableQuery);

export default createTable;
