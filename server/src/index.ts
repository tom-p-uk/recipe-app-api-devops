import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import morgan from 'morgan';

import routes from './routes';
import dbClient from './store/dbClient';
import createTable from "./store/createTable";

const app = express();

const PORT = process.env.APP_PORT || 8000;

dbClient.connect()
    .then(() => createTable())
    .then(() =>  console.log('Connected to Postgres and created table `todos`'))
    .catch(err => console.log(err));

app.use(morgan('combined'));
app.use(cors());
app.use(bodyParser.json());
app.use(routes);

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
